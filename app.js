
const fs = require('fs')
const _ = require('lodash')
const yargs = require('yargs')

//console.log(yargs.argv)

const notes= require('./notes.js')

const  titleCommandOPtions = {
  describe : 'Title of a note',
  demand : true,
  alias : 't'
};

const bodyCommandOPtions = {
  describe : 'Body of a Note',
  demand : true,
  alias : 'b'
}

const argv = yargs
  .command('add','Add a new note',{
    title : titleCommandOPtions,
    body : bodyCommandOPtions
  })
  .command('list','List Saved Notes')
  .command('read','Read a note with specific title',{
    title : titleCommandOPtions
  })
  .command('remove','Remove a note with specific title',{
    title : titleCommandOPtions
  })
  .help()
  .argv;
var command = argv._[0];

if(command === 'add'){
  notes.addNotes(argv.title,argv.body)
}
else if(command === 'list'){
  notes.getAll();
}
else if(command === 'read'){
  notes.getNote(argv.title)
}
else if(command === 'remove'){
  notes.removeNote(argv.title)
}
else{
  console.log('command not recocnized.')
  console.log('type " node app.js help " to get all command list.')
}
