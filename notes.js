const fs = require('fs');

/*************** Fetch all the Note and Deals with reading JSON File ****************/
var fetchNotes = () => {
    try {
      var noteString = fs.readFileSync('notes-data.json')
        return JSON.parse(noteString)
    } catch (e) {
        return [];
    }
}
/***************************************/


/*************** Create a New File or add JSON into it ****************/
var saveNotes = (notes) => {
  fs.writeFileSync('notes-data.json',JSON.stringify(notes))
}
/***************************************/


/******************* Display The Note ********************/
var displayNote = (note) => {
  //debugger;
  console.log('-----------------------------')
  console.log(`Title : ${note.title}`)
  console.log(`Body : ${note.body}`)
  console.log('-----------------------------')
}
/***************************************/


/***********************************CRUD***********************************************/

/*****************Add Note**********************/
var addNotes = (title,body) => {
  var notes = fetchNotes();
  var note = {
    title,
    body
  }

  var duplicateNote = notes.filter((note)=> note.title === title)

  if(duplicateNote.length === 0){
      notes.push(note)
      console.log('Note Succesfully Created')
      saveNotes(notes)
  }else{
    console.log('Title already exist')
    note =duplicateNote[0]
  }
  displayNote(note);
}
/***************************************/


/****************** List All Note *********************/
var listNotes = () =>{
  console.log('list notes')
    var duplicateNote = notes.filter((note)=> note.title === title) // check if duplicate note exist
}
var getAll = () =>{
  console.log(`Prining ${fetchNotes().length} note(s).`)
  fetchNotes().forEach((note)=>displayNote(note))

}
/***************************************/

/******************** Get A Specific Note *******************/
var getNote = (title) =>{
  var notes = fetchNotes();
  var noteWithTitle = notes.filter((note)=> note.title === title)
  if(noteWithTitle.length === 0){
    console.log('Note Not Found')
    console.log('Look for all notes with "node app.js list"')
  }else{
    note = noteWithTitle[0]
    displayNote(note)
  }
}
/***************************************/


/****************** Remove A Note *********************/
var removeNote = (title) =>{
var notes = fetchNotes();
var noteExcepteGivenTitle = notes.filter((note)=> note.title !== title)
  if(notes.length === noteExcepteGivenTitle.length){
    console.log('No note with given title found')
  }else{
    saveNotes(noteExcepteGivenTitle);
    console.log('Note Succesfully deleted.')
  }
}
/***************************************/


/***********************************CRUD***********************************************/



module.exports = {
  addNotes,
  listNotes,
  getAll,
  getNote,
  removeNote
}
